// Package sort provides primitives for sorting slices and user-defined
// collections.
package general

import (
	"strconv"
)

// Fprint formats using the default formats for its operands and writes to w.
// Spaces are added between operands when neither is a string.
// It returns the number of bytes written and any write error encountered.
func Round(val float64) int {
	if val < 0 { return int(val-0.5) }
	return int(val+0.5)
}

func IsNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}
